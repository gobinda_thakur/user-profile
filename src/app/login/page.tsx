"use client";

import styles from "./login.module.css";
import LoginScreen from "./components/login-screen";

const Login = () => {
    return (
        <main className={styles.container}>
            <LoginScreen />
        </main>
    );
};

export default Login;
