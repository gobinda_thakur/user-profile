"use client";

import { useSearchParams, usePathname, redirect } from "next/navigation";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import LoginForm from "@/components/login-form";
import { useFormik } from "formik";
import { loginValidationSchema } from "@/utils/user-schema";
import { isValidCredential, storeCredentials } from "@/utils/auth";
import { useState, useEffect } from "react";
import Alert from "@mui/material/Alert";

const LoginScreen = () => {
    const [error, setError] = useState("");
    const searchParams = useSearchParams();
    const pathname = usePathname();
    const [loginSuccess, setLoginSuccess] = useState(false);
    useEffect(() => {
        const initialUsername = searchParams.get("initialUsername");
        const initialPassword = searchParams.get("initialPassword");
        if (initialUsername && initialPassword) {
            storeCredentials(initialUsername, initialPassword);
            redirect("/login");
        }
    }, [pathname, searchParams]);
    const formik = useFormik({
        initialValues: {
            email: "",
            password: "",
        },
        validationSchema: loginValidationSchema,
        onSubmit: (values) => {
            const success = isValidCredential(values);
            if (success) {
                setError("");
                setLoginSuccess(true);
            } else {
                setError("Invalid credentials");
                setLoginSuccess(false);
            }
        },
    });

    useEffect(() => {
        if (loginSuccess) {
            redirect("/home");
        }
    }, [loginSuccess]);

    return (
        <Card variant="outlined">
            <CardContent>
                <Typography color="text.primary" variant="h5" gutterBottom>
                    Login
                </Typography>
                <div style={{ padding: 5, margin: 5 }}>
                    <LoginForm formik={formik}>
                        <Button variant="contained" type="submit" fullWidth>
                            Login
                        </Button>
                    </LoginForm>
                </div>
                {!!error ? (
                    <Alert variant="filled" severity="error">
                        {error}
                    </Alert>
                ) : null}
            </CardContent>
        </Card>
    );
};

export default LoginScreen;
