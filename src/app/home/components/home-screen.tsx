"use client";

import { useState } from "react";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import DataTable from "@/components/table";
import Paper from "@mui/material/Paper";
import styles from "../home.module.css";
import Container from "@mui/material/Container";
import Button from "@mui/material/Button";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "@/redux/store";
import { extractKeysFromObject } from "@/utils/common";
import { selectPerson, addPerson } from "@/redux/features/person-slice";
import { useRouter } from "next/navigation";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import AppDialog from "@/components/dialog";
import PersonForm from "@/components/person-from";
import { useFormik } from "formik";
import { personValidationSchema } from "@/utils/user-schema";

const HomeScreen = () => {
    const [open, setOpen] = useState(false);
    const personList = useSelector((state: RootState) => state.personReducer.users);
    const dispatch = useDispatch<AppDispatch>();
    const router = useRouter();
    const rowList = extractKeysFromObject(personList);
    const formik = useFormik({
        initialValues: {
            name: "",
            age: null,
            profession: "",
        },
        validationSchema: personValidationSchema,
        onSubmit: (values) => {
            dispatch(addPerson(values));
            setOpen(false);
        },
    });
    const handleRowClick = (personId: any) => {
        dispatch(selectPerson(personId));
        router.push("/home/profile");
    };
    const openAddPersonForm = () => {
        setOpen(true);
    };
    const closeAddPersonForm = () => {
        setOpen(false);
    };
    return (
        <Container fixed>
            <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
                <Grid item xs={12}>
                    <Breadcrumbs aria-label="breadcrumb">
                        <Typography color="text.primary">Home</Typography>
                    </Breadcrumbs>
                </Grid>
                <Grid item xs={12}>
                    <Paper className={styles.actionbar}>
                        <Grid container direction="row" justifyContent="flex-end" alignItems="center">
                            <Grid item xs={10}>
                                <Typography variant="subtitle1" gutterBottom>
                                    Person List
                                </Typography>
                            </Grid>
                            <Grid item xs={2}>
                                <Button variant="contained" onClick={openAddPersonForm}>
                                    Add a person
                                </Button>
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
                <Grid item xs={12}>
                    <DataTable columnName={rowList} data={personList} handleRowClick={handleRowClick} />
                </Grid>
            </Grid>
            <AppDialog open={open} handleClose={closeAddPersonForm} title="Add a person">
                <PersonForm formik={formik}>
                    <Button variant="contained" type="submit" fullWidth>
                        Save
                    </Button>
                </PersonForm>
            </AppDialog>
        </Container>
    );
};

export default HomeScreen;
