"use client";

import { useState } from "react";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import styles from "../home.module.css";
import Container from "@mui/material/Container";
import Button from "@mui/material/Button";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "@/redux/store";
import { editPerson, Person } from "@/redux/features/person-slice";
import { useRouter } from "next/navigation";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import AppDialog from "@/components/dialog";
import PersonForm from "@/components/person-from";
import { useFormik } from "formik";
import { personValidationSchema } from "@/utils/user-schema";

const ProfileScreen = () => {
    const [open, setOpen] = useState(false);
    const personDetails = useSelector((state: RootState) => state.personReducer.selectedUser);
    const dispatch = useDispatch<AppDispatch>();
    const router = useRouter();
    const formik = useFormik({
        initialValues: personDetails || {},
        validationSchema: personValidationSchema,
        onSubmit: (values) => {
            dispatch(
                editPerson({
                    id: personDetails?.id,
                    ...values,
                })
            );
            setOpen(false);
        },
    });
    const handleBack = () => {
        router.push("/home");
    };
    const openEditPersonForm = () => {
        setOpen(true);
    };
    const closeEditPersonForm = () => {
        setOpen(false);
    };
    return (
        <Container fixed>
            <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
                <Grid item xs={12}>
                    <Breadcrumbs aria-label="breadcrumb">
                        <Typography>Home</Typography>
                        <Typography color="text.primary">Profile</Typography>
                    </Breadcrumbs>
                </Grid>
                <Grid item xs={12}>
                    <Paper className={styles.actionbar}>
                        <Grid container direction="row" justifyContent="flex-end" alignItems="center">
                            <Grid item xs={10}>
                                <Typography variant="subtitle1" gutterBottom>
                                    Person Details
                                </Typography>
                            </Grid>
                            <Grid item xs={2}>
                                <Button variant="contained" onClick={openEditPersonForm}>
                                    Edit
                                </Button>
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
                <Grid item xs={12}>
                    <Card variant="outlined">
                        <CardContent>
                            {personDetails &&
                                Object.keys(personDetails).map((person) => {
                                    if (person !== "id") {
                                        return (
                                            <Grid
                                                container
                                                direction="row"
                                                justifyContent="flex-end"
                                                alignItems="center"
                                                key={person}
                                            >
                                                <Grid item xs={6}>
                                                    <Typography variant="subtitle2" gutterBottom color="text.secondary">
                                                        {person.toUpperCase()}
                                                    </Typography>
                                                </Grid>
                                                <Grid item xs={6}>
                                                    <Typography variant="body2" gutterBottom>
                                                        {personDetails[person as keyof Person]}
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        );
                                    }
                                })}
                        </CardContent>
                        <CardActions>
                            <Button variant="outlined" onClick={handleBack}>
                                Back
                            </Button>
                        </CardActions>
                    </Card>
                </Grid>
            </Grid>
            <AppDialog open={open} handleClose={closeEditPersonForm} title="Edit a person">
                <PersonForm formik={formik}>
                    <Button variant="contained" type="submit" fullWidth>
                        Save
                    </Button>
                </PersonForm>
            </AppDialog>
        </Container>
    );
};

export default ProfileScreen;
