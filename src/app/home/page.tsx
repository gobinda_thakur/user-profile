"use client";

import isAuth from "@/components/is-auth";
import styles from "./home.module.css";
import HomeScreen from "./components/home-screen";

const Home = () => {
    return (
        <main className={styles.container}>
            <HomeScreen />
        </main>
    );
};

export default isAuth(Home);
