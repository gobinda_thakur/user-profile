"use client"

import ProfileScreen from "../components/profile-screen";
import styles from '../home.module.css';

const Profile = () => {
  return (
    <main className={styles.container}>
      <ProfileScreen />
    </main>
  );
};

export default Profile;