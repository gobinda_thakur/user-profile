import * as React from "react";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useTheme } from "@mui/material/styles";

type AppDialogProps = {
    title: string;
    subTitle?: string | undefined;
    children: string | React.JSX.Element | React.JSX.Element[] | React.ReactNode;
    open: boolean;
    handleClose: () => void;
};
const AppDialog = ({ open, handleClose, title, subTitle, children }: AppDialogProps) => {
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down("md"));
    return (
        <Dialog open={open} onClose={handleClose} fullScreen={fullScreen} aria-labelledby="responsive-dialog-title">
            <DialogTitle>{title}</DialogTitle>
            <DialogContent style={{ padding: "0.5rem" }}>
                {subTitle ? <DialogContentText>{subTitle}</DialogContentText> : null}
                {children}
            </DialogContent>
        </Dialog>
    );
};

export default AppDialog;
