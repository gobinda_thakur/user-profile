"use client";
import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import AdbIcon from "@mui/icons-material/Adb";
import { handleLogout } from "@/utils/auth";
import { useRouter } from "next/navigation";

function Navbar() {
    const router = useRouter();
    const logout = () => {
        handleLogout();
        router.push("/login");
    };
    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar>
                    <AdbIcon sx={{ display: { xs: "none", md: "flex" }, mr: 1 }} />
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        Users
                    </Typography>
                    <Button color="inherit" onClick={() => logout()}>
                        Logout
                    </Button>
                </Toolbar>
            </AppBar>
        </Box>
    );
}
export default Navbar;
