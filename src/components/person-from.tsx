import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";

const PersonForm = ({ formik, children }: any) => {
    return (
        <form onSubmit={formik.handleSubmit} noValidate>
            <Grid container rowSpacing={1}>
                <Grid item xs={12}>
                    <TextField
                        fullWidth
                        required
                        id="name"
                        name="name"
                        label="Name"
                        value={formik.values.name}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.name && Boolean(formik.errors.name)}
                        helperText={formik.touched.name && formik.errors.name}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        fullWidth
                        required
                        id="age"
                        name="age"
                        label="Age"
                        type="number"
                        value={formik.values.age}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.age && Boolean(formik.errors.age)}
                        helperText={formik.touched.age && formik.errors.age}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        fullWidth
                        required
                        id="profession"
                        name="profession"
                        label="Profession"
                        value={formik.values.profession}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.profession && Boolean(formik.errors.profession)}
                        helperText={formik.touched.profession && formik.errors.profession}
                    />
                </Grid>
                <Grid item xs={12}>
                    {children}
                </Grid>
            </Grid>
        </form>
    );
};

export default PersonForm;
