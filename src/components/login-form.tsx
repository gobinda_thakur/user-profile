import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";

const LoginForm = ({ formik, children }: any) => {
    return (
        <form onSubmit={formik.handleSubmit} noValidate>
            <Grid container rowSpacing={2}>
                <Grid item xs={12}>
                    <TextField
                        fullWidth
                        required
                        id="email"
                        name="email"
                        label="Email"
                        value={formik.values.email}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.email && Boolean(formik.errors.email)}
                        helperText={formik.touched.email && formik.errors.email}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        fullWidth
                        required
                        id="password"
                        name="password"
                        label="Password"
                        type="password"
                        value={formik.values.password}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.password && Boolean(formik.errors.password)}
                        helperText={formik.touched.password && formik.errors.password}
                    />
                </Grid>
                <Grid item xs={12}>
                    {children}
                </Grid>
            </Grid>
        </form>
    );
};

export default LoginForm;
