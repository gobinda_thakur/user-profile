"use client";

type credentialProps = {
    email: string,
    password: string
};

//export const isAuthenticated = true;

export function isValidCredential(credential: credentialProps) {
    if (typeof window !== 'undefined') {
        const storedEmail = localStorage.getItem('initialUsername');
        const storedPassword = localStorage.getItem('initialPassword');
        if (credential.email === storedEmail && credential.password === storedPassword) {
            // Set loggedIn flag in localStorage
            localStorage.setItem('loggedIn', 'true');
            return true;
        }
    }
    return false;
}

export function storeCredentials(initialUsername: string, initialPassword: string) {
    if (typeof window !== 'undefined') {
        localStorage.setItem('initialUsername', initialUsername);
        localStorage.setItem('initialPassword', initialPassword);
    }
}

export function isAuthenticated() {
    if (typeof window !== 'undefined') {
        const auth = localStorage.getItem('loggedIn');
        return auth;

    }
    return false;
}

export function handleLogout() {
    if (typeof window !== 'undefined') {
        localStorage.removeItem('loggedIn');
        localStorage.removeItem('initialUsername');
        localStorage.removeItem('initialPassword');
    }
}