export function extractKeysFromObject(personList: any) {
    if (!personList) {
        return [];
    }
    return Object.keys(personList[0]).reduce((acc: any, k) => {
        if (k !== "id") {
            acc = [
                ...acc,
                k.toUpperCase()
            ];
        }
        return acc;
    }, []);
}