import { configureStore } from "@reduxjs/toolkit";
import personReducer from "./features/person-slice";

export const store = configureStore({
    reducer: {
        personReducer
    },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;