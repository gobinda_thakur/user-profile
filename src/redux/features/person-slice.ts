import { createSlice } from "@reduxjs/toolkit";
import { nanoid } from 'nanoid';

export type Person = {
    id: string;
    name: string;
    age: number;
    profession: string;
};

type ProfileState = {
    users: Person[];
    selectedUser: Person | null;
};

const initialState: ProfileState = {
    users: [
        {
            id: nanoid(),
            name: "Gobinda Thakur",
            age: 34,
            profession: "Software Professional"
        },
        {
            id: nanoid(),
            name: "Jyotirekha Dehury",
            age: 33,
            profession: "Doctor"
        },
        {
            id: nanoid(),
            name: "Sachin Tendulkar",
            age: 50,
            profession: "Sports Person"
        },
        {
            id: nanoid(),
            name: "Naveen Patnaik",
            age: 77,
            profession: "Politician"
        }
    ],
    selectedUser: null
};

export const person = createSlice({
    name: "person",
    initialState,
    reducers: {
        addPerson: (state, action) => {
            const newPerson = {
                id: nanoid(),
                ...action.payload
            }
            state.users.push(newPerson);
        },
        editPerson: (state, action) => {
            const newPersonList = state.users.reduce((acc: any[], curr) => {
                if (curr.id === action.payload.id) {
                    acc = [
                        ...acc,
                        {
                            ...action.payload
                        }
                    ];
                } else {
                    acc = [
                        ...acc,
                        {
                            ...curr
                        }
                    ];
                }
                return acc;
            }, []);
            state.users = newPersonList;
            state.selectedUser = action.payload;
        },
        removePerson: (state, action) => {
            state.users = state.users.filter((person) => person.id !== action.payload);
        },
        selectPerson: (state, action) => {
            const selectedPerson = state.users.find((person) => person.id === action.payload);
            if (selectedPerson) {
                state.selectedUser = selectedPerson;
            }
        },
    },
});

export const { addPerson, editPerson, removePerson, selectPerson } = person.actions;
export default person.reducer;