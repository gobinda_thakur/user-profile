/** @type {import('next').NextConfig} */
const nextConfig = {
    async redirects() {
        return [
            {
                source: "/", // automatically becomes /docs/with-basePath
                destination: "/login", // automatically becomes /docs/another
                permanent: false,
            },
        ];
    },
};

module.exports = nextConfig;
